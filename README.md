**Nodejs Deployment **

Project name is a Nodejs that allows client  to run the Hello_world Container.


**Prerequisites**

Before you begin, ensure you have met the following requirements:


- You have installed the latest version of minikube, Helm and Docker
- You have a </Linux/Mac> machine.


**Installing Nodejs**

To install Nodejs, follow these steps:

Linux or macOS:

_#minikube start_

_#minikube dashboard_

Test the minikube cluster is running

_#kubectl config view_

_#kubectl get pod,svc -n kube-system_


**Running the Pipeline on the gitlab**

- Craete a runner on the Linux or macOS from the below link
- Make sure the gitlab-runner name is test-runner

https://docs.gitlab.com/runner/install/linux-manually.html

https://docs.gitlab.com/runner/install/osx.html


**Running Nodejs**

To run Nodejs, follow these steps:

_#minikube service hello-node_

- Click the plus sign, and then click Select port to view on Host 1
- Type 30000 (see port opposite to 8080 in services output), and then click. This opens up a browser window that serves your app and shows the “Hello World” message.


